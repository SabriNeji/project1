package fr.formation.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.PreparedStatement;

import fr.formation.buisness.beans.Formation;
import fr.formation.buisness.beans.PersonneDao;



public class DaoImpl implements IDao{
	
	
	 private static DaoImpl dao = null;

	 public static DaoImpl getInstance(){
			
			if(dao==null){
				dao = new DaoImpl();
			}
			
			return dao;
		}

	@Override
	public  PersonneDao createPersonne(PersonneDao personne) throws SQLException {
		PreparedStatement preparedStatement=null;
		ResultSet rs = null ;
		Connection	connection =  ConnectionUtil.getInstance().getConnection();
		 
			try {
				
				preparedStatement = (PreparedStatement) connection.prepareStatement("insert into PERSONNE(NOM,PRENOM,EMAIL,MDP) values (?, ?, ?, ? )",Statement.RETURN_GENERATED_KEYS);
	            // Parameters start with 1
	            preparedStatement.setString(1, personne.getNom());
	            preparedStatement.setString(2, personne.getPrenom());
	            preparedStatement.setString (3, personne.getEmail());
	            preparedStatement.setString (4, personne.getMdp());
	            preparedStatement.executeUpdate();
	           
	            int id =0;
	             rs = preparedStatement.getGeneratedKeys();
	            
	            		 
	            if (rs != null && rs.next()) {
	                id = rs.getInt(1);
	            }
	            
	            System.out.println(" lid genere " + id );
	          
	            personne.setId(id);
	           
			} catch (SQLException e) {
	            throw new DaoException(e.getMessage(), 1);
	        }finally{
	       
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlex) {
					// ignore, as we can't do anything about it here
				}

				rs = null;
			}

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException sqlex) {
					// ignore, as we can't do anything about it here
				}

				preparedStatement = null;
			}

			if (connection != null) {
				try {
					ConnectionUtil.getInstance().close(connection);
				} catch (SQLException sqlex) {
					// ignore, as we can't do anything about it here
				}

				connection = null;
				// if (rs != null) rs.close();
				// if (preparedStatement != null) preparedStatement.close();
				// if (connection != null) connection.close();
			}
		}
			return personne;
		
	}
	
	public  List<PersonneDao> getAllPersonnes() throws SQLException{
		List<PersonneDao> liste = new ArrayList<PersonneDao>();
		  ResultSet rs = null ;
		  PreparedStatement preparedStatement = null;
		  Connection	connection =  ConnectionUtil.getInstance().getConnection();
		  try {	
			  	preparedStatement = (PreparedStatement) connection.prepareStatement("select * from personne");
	             rs = preparedStatement.executeQuery();
	           
			  	//ica ajout
			  //	rs = connection.createStatement().executeQuery("select * from personne");
	           
	            while (rs.next()) {
	            PersonneDao user = new PersonneDao();
	                user.setId (rs.getInt("id"));
	                user.setNom (rs.getString("nom"));
	                user.setPrenom(rs.getString("prenom"));
	                user.setEmail(rs.getString("email"));
	            
	                liste.add(user);
	               
	            }
	            
	            rs.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }finally{
	 	       
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException sqlex) {
						// ignore, as we can't do anything about it here
					}

					rs = null;
				}

				if (preparedStatement != null) {
					try {
						preparedStatement.close();
					} catch (SQLException sqlex) {
						// ignore, as we can't do anything about it here
					}

					preparedStatement = null;
				}

				if (connection != null) {
					try {
						ConnectionUtil.getInstance().close(connection);
					} catch (SQLException sqlex) {
						// ignore, as we can't do anything about it here
					}

					connection = null;
					// if (rs != null) rs.close();
					// if (preparedStatement != null) preparedStatement.close();
					// if (connection != null) connection.close();
				}
	        }
		return liste;
		
	}

	@Override
	public PersonneDao findPersonneByUtilisateur(PersonneDao p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Formation> getListeFormationParPersonne(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void creationFormation(Formation f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Formation> recupererAllFormation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void inscriptionFormation(int idPersonne, int idFormation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Formation> getListeFormationParFormateur(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}